#!/bin/bash
#
# make zip file and template README for this project
# generate gerbers with "use proper filename extensions" UNCHECKED
# so all the gerber file names end in ".pho"
#
base="G2-LOGIC_DB-revB"
orig="LogicBoard_DB"
mv ${orig}-B_Cu.pho ${base}-Layer4.pho
mv ${orig}-B_Mask.pho ${base}-B_Mask.pho
mv ${orig}-B_Paste.pho ${base}-B_Paste.pho
mv ${orig}-B_SilkS.pho ${base}-B_SilkS.pho
mv ${orig}.drl ${base}.drl
mv ${orig}-drl_map.pho ${base}-drl_map.pho
mv ${orig}-Dwgs_User.pho ${base}-Fab_dwg.pho
mv ${orig}-F_Cu.pho ${base}-Layer1.pho
mv ${orig}-F_Mask.pho ${base}-F_Mask.pho
mv ${orig}-F_Paste.pho ${base}-F_Paste.pho
mv ${orig}-F_SilkS.pho ${base}-F_SilkS.pho
mv ${orig}-Inner1_Cu.pho ${base}-Layer3.pho
mv ${orig}-Inner2_Cu.pho ${base}-Layer2.pho
#mv ${orig}-NPTH.drl ${base}-NPTH.drl
#mv ${orig}-NPTH-drl_map.pho ${base}-NPTH-drl_map.pho
