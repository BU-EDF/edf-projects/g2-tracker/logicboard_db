PCBNEW-LibModule-V1  Thu 20 Nov 2014 01:53:55 PM EST
# encoding utf-8
Units mm
$INDEX
Ina202-VSSOP
$EndINDEX
$MODULE Ina202-VSSOP
Po 0 0 0 15 546E38AD 00000000 ~~
Li Ina202-VSSOP
Sc 0
AR 
Op 0 0 0
T0 0 -3.91 1 1 0 0.15 N V 21 N "Ina202-VSSOP"
T1 0 4.26 1 1 0 0.15 N V 21 N "VAL**"
DS -1.5 -1.51 -1.5 1.5 0.15 21
DS -1.5 -1.51 -1.41 -1.51 0.15 21
DS -1.5 1.51 -1.41 1.51 0.15 21
DS 1.5 1.51 1.41 1.51 0.15 21
DS 1.5 -1.51 1.41 -1.51 0.15 21
DS 1.5 -1.51 1.5 1.5 0.15 21
$PAD
Sh "1" R 0.45 1.45 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -0.975 2.2
.SolderMask 0.05
.LocalClearance 0.127
$EndPAD
$PAD
Sh "2" R 0.45 1.45 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -0.325 2.2
.SolderMask 0.05
.LocalClearance 0.127
$EndPAD
$PAD
Sh "3" R 0.45 1.45 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0.325 2.2
.SolderMask 0.05
.LocalClearance 0.127
$EndPAD
$PAD
Sh "4" R 0.45 1.45 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0.975 2.2
.SolderMask 0.05
.LocalClearance 0.127
$EndPAD
$PAD
Sh "5" R 0.45 1.45 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0.975 -2.2
.SolderMask 0.05
.LocalClearance 0.127
$EndPAD
$PAD
Sh "6" R 0.45 1.45 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0.325 -2.2
.SolderMask 0.05
.LocalClearance 0.127
$EndPAD
$PAD
Sh "7" R 0.45 1.45 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -0.325 -2.2
.SolderMask 0.05
.LocalClearance 0.127
$EndPAD
$PAD
Sh "8" R 0.45 1.45 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -0.975 -2.2
.SolderMask 0.05
.LocalClearance 0.127
$EndPAD
$EndMODULE Ina202-VSSOP
$EndLIBRARY
