EESchema Schematic File Version 2
LIBS:2x14_conn
LIBS:ad7998
LIBS:bav99
LIBS:ds18b20u
LIBS:ina202a
LIBS:lt3015eq
LIBS:mcp23017-qfn
LIBS:opa703
LIBS:power
LIBS:pspice
LIBS:sn74hc05d
LIBS:tps73501drbt
LIBS:tps78630kttt
EELAYER 24 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 15
Title ""
Date "18 nov 2014"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 8400 1550 1100 900 
U 5476952B
F0 "Monitoring" 50
F1 "sch/DB_monitoring.sch" 50
F2 "SDA" B L 8400 2350 60 
F3 "SCL" I L 8400 2250 60 
F4 "TDC_I" I L 8400 1700 60 
F5 "3V_I[3..0]" I L 8400 1900 60 
F6 "-3V_I[3..0]" I L 8400 2100 60 
F7 "1V4_I" I L 8400 2000 60 
$EndSheet
$Sheet
S 5050 800  1400 1350
U 547650A6
F0 "ASDQ power" 50
F1 "sch/DB_Power.sch" 50
F2 "_EN_3V" I L 5050 900 60 
F3 "_EN_1V4" I L 5050 1000 60 
F4 "Reset3V" I L 5050 1500 60 
F5 "Reset1V4" I L 5050 1600 60 
F6 "3V_I[3..0]" O R 6450 900 60 
F7 "3V_V[3..0]" O R 6450 1000 60 
F8 "_EN_-3V" I L 5050 1100 60 
F9 "Reset-3V" I L 5050 1700 60 
F10 "-3V_I[3..0]" O R 6450 1200 60 
F11 "-3V_V[3..0]" O R 6450 1300 60 
F12 "1V4_I" O R 6450 1500 60 
F13 "1V4_V" O R 6450 1600 60 
F14 "3V_OC[3..0]" O R 6450 1800 60 
F15 "-3V_OC[3..0]" O R 6450 2000 60 
F16 "1V4_OC" O R 6450 1900 60 
$EndSheet
Text Label 9000 1000 0    60   ~ 0
TDC_OC
Text Label 9000 900  0    60   ~ 0
TDC_I
$Sheet
S 8400 800  550  400 
U 546954CD
F0 "ASDQ1V4 sense" 50
F1 "sch/Current_sense.sch" 50
F2 "_Reset" I L 8400 1150 60 
F3 "-" I L 8400 950 60 
F4 "+" I L 8400 850 60 
F5 "V_I" O R 8950 900 60 
F6 "OC" O R 8950 1000 60 
$EndSheet
Text Label 8150 1700 0    60   ~ 0
TDC_I
$Comp
L MCP23017-QFN U1
U 1 1 547604D9
P 3400 3550
F 0 "U1" H 3850 2500 60  0000 C CNN
F 1 "MCP23017-QFN" V 3400 3550 60  0000 C CNN
F 2 "" H 3200 3850 60  0000 C CNN
F 3 "" H 3200 3850 60  0000 C CNN
	1    3400 3550
	1    0    0    -1  
$EndComp
Text Label 4250 3950 0    60   ~ 0
3V_OC3
Text Label 4250 3850 0    60   ~ 0
3V_OC2
Text Label 4250 4150 0    60   ~ 0
3V_OC1
Text Label 4250 4050 0    60   ~ 0
3V_OC0
Text Label 4250 3050 0    60   ~ 0
-3V_OC3
Text Label 4250 2850 0    60   ~ 0
-3V_OC2
Text Label 4250 3250 0    60   ~ 0
-3V_OC1
Text Label 4250 3150 0    60   ~ 0
-3V_OC0
Text Label 4250 4250 0    60   ~ 0
1V4_OC
Text Label 4250 3750 0    60   ~ 0
TDC_OC
Text Label 6450 1800 0    60   ~ 0
3V_OC[3..0]
Text Label 6450 2000 0    60   ~ 0
-3V_OC[3..0]
Text Label 6450 1900 0    60   ~ 0
1V4_OC
Text Label 6450 900  0    60   ~ 0
3V_I[3..0]
Text Label 6450 1000 0    60   ~ 0
3V_V[3..0]
Text Label 6450 1200 0    60   ~ 0
-3V_I[3..0]
Text Label 6450 1300 0    60   ~ 0
-3V_V[3..0]
Text Label 6450 1500 0    60   ~ 0
1V4_I
Text Label 6450 1600 0    60   ~ 0
1V4_V
Text Label 7850 1900 0    60   ~ 0
3V_I[3..0]
Text Label 7850 2000 0    60   ~ 0
1V4_I
Text Label 7850 2100 0    60   ~ 0
-3V_I[3..0]
$Comp
L +3.3V #PWR01
U 1 1 5477F8F7
P 3400 1800
F 0 "#PWR01" H 3400 1760 30  0001 C CNN
F 1 "+3.3V" H 3400 1910 30  0000 C CNN
F 2 "" H 3400 1800 60  0000 C CNN
F 3 "" H 3400 1800 60  0000 C CNN
	1    3400 1800
	1    0    0    -1  
$EndComp
$Comp
L C C1
U 1 1 5477FAF1
P 3750 2150
F 0 "C1" V 3850 2300 50  0000 C CNN
F 1 "0.1uF" V 3850 2000 50  0000 C CNN
F 2 "" H 3750 2150 60  0000 C CNN
F 3 "" H 3750 2150 60  0000 C CNN
	1    3750 2150
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR02
U 1 1 5477FDA6
P 3750 2450
F 0 "#PWR02" H 3750 2450 30  0001 C CNN
F 1 "GND" H 3750 2380 30  0001 C CNN
F 2 "" H 3750 2450 60  0000 C CNN
F 3 "" H 3750 2450 60  0000 C CNN
	1    3750 2450
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR03
U 1 1 54780572
P 3400 4950
F 0 "#PWR03" H 3400 4950 30  0001 C CNN
F 1 "GND" H 3400 4880 30  0001 C CNN
F 2 "" H 3400 4950 60  0000 C CNN
F 3 "" H 3400 4950 60  0000 C CNN
	1    3400 4950
	1    0    0    -1  
$EndComp
$Comp
L 2X14_CONN J2
U 1 1 54780E48
P 1750 6300
F 0 "J2" H 1750 5500 60  0000 C CNN
F 1 "2X14_CONN" V 1750 6300 60  0001 C CNN
F 2 "" H 1950 5800 60  0000 C CNN
F 3 "" H 1950 5800 60  0000 C CNN
	1    1750 6300
	1    0    0    -1  
$EndComp
$Comp
L 2X14_CONN J1
U 1 1 54781042
P 9350 4900
F 0 "J1" H 9350 4100 60  0000 C CNN
F 1 "2X14_CONN" V 9350 4900 60  0001 C CNN
F 2 "" H 9550 4400 60  0000 C CNN
F 3 "" H 9550 4400 60  0000 C CNN
	1    9350 4900
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR04
U 1 1 5478123C
P 2500 6700
F 0 "#PWR04" H 2500 6790 20  0001 C CNN
F 1 "+5V" H 2500 6790 30  0000 C CNN
F 2 "" H 2500 6700 60  0000 C CNN
F 3 "" H 2500 6700 60  0000 C CNN
	1    2500 6700
	-1   0    0    -1  
$EndComp
Wire Wire Line
	8950 900  9350 900 
Wire Wire Line
	8950 1000 9350 1000
Wire Wire Line
	7750 850  8400 850 
Wire Wire Line
	7750 950  8400 950 
Wire Wire Line
	8150 1700 8400 1700
Wire Wire Line
	4250 3950 4650 3950
Wire Wire Line
	4250 3850 4650 3850
Wire Wire Line
	4250 4150 4650 4150
Wire Wire Line
	4250 4050 4650 4050
Wire Wire Line
	4250 3050 4650 3050
Wire Wire Line
	4250 2850 4650 2850
Wire Wire Line
	4250 3250 4650 3250
Wire Wire Line
	4250 3150 4650 3150
Wire Wire Line
	4250 4250 4650 4250
Wire Wire Line
	4250 3750 4650 3750
Wire Bus Line
	6450 1800 7000 1800
Wire Bus Line
	6450 2000 7000 2000
Wire Wire Line
	6450 1900 7000 1900
Wire Bus Line
	6450 900  7000 900 
Wire Bus Line
	6450 1000 7000 1000
Wire Bus Line
	6450 1200 7000 1200
Wire Bus Line
	6450 1300 7000 1300
Wire Wire Line
	6450 1500 7000 1500
Wire Wire Line
	6450 1600 7000 1600
Wire Bus Line
	7850 1900 8400 1900
Wire Wire Line
	4600 1100 5050 1100
Wire Wire Line
	7850 2000 8400 2000
Wire Bus Line
	7850 2100 8400 2100
Wire Wire Line
	3400 1800 3400 2300
Wire Wire Line
	3750 1900 3400 1900
Connection ~ 3400 1900
Wire Wire Line
	3750 2400 3750 2450
Wire Wire Line
	3400 4800 3400 4950
Wire Wire Line
	2250 5550 2250 5850
Wire Wire Line
	2250 5750 2150 5750
Wire Wire Line
	2150 5650 2600 5650
Connection ~ 2250 5650
$Comp
L +5V #PWR05
U 1 1 54781D70
P 1150 6750
F 0 "#PWR05" H 1150 6840 20  0001 C CNN
F 1 "+5V" H 1150 6840 30  0000 C CNN
F 2 "" H 1150 6750 60  0000 C CNN
F 3 "" H 1150 6750 60  0000 C CNN
	1    1150 6750
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1250 5750 1350 5750
$Comp
L GND #PWR06
U 1 1 547820FB
P 2350 6050
F 0 "#PWR06" H 2350 6050 30  0001 C CNN
F 1 "GND" H 2350 5980 30  0001 C CNN
F 2 "" H 2350 6050 60  0000 C CNN
F 3 "" H 2350 6050 60  0000 C CNN
	1    2350 6050
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR07
U 1 1 54782160
P 1150 6050
F 0 "#PWR07" H 1150 6050 30  0001 C CNN
F 1 "GND" H 1150 5980 30  0001 C CNN
F 2 "" H 1150 6050 60  0000 C CNN
F 3 "" H 1150 6050 60  0000 C CNN
	1    1150 6050
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2350 5950 2350 6050
Wire Wire Line
	2150 5950 2600 5950
Wire Wire Line
	1150 5950 1350 5950
Wire Wire Line
	1150 5950 1150 6050
Connection ~ 1250 5950
Wire Wire Line
	1350 6050 1250 6050
Connection ~ 1250 6050
Connection ~ 2250 5950
Wire Wire Line
	2250 6150 2150 6150
Wire Wire Line
	2250 6050 2150 6050
Connection ~ 2250 6050
$Comp
L -5V #PWR08
U 1 1 54782B56
P 1250 5550
F 0 "#PWR08" H 1250 5690 20  0001 C CNN
F 1 "-5V" H 1250 5660 30  0000 C CNN
F 2 "" H 1250 5550 60  0000 C CNN
F 3 "" H 1250 5550 60  0000 C CNN
	1    1250 5550
	-1   0    0    -1  
$EndComp
$Comp
L -5V #PWR09
U 1 1 54782BBB
P 2250 5550
F 0 "#PWR09" H 2250 5690 20  0001 C CNN
F 1 "-5V" H 2250 5660 30  0000 C CNN
F 2 "" H 2250 5550 60  0000 C CNN
F 3 "" H 2250 5550 60  0000 C CNN
	1    2250 5550
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2150 6750 2700 6750
Wire Wire Line
	2250 6750 2250 6950
Wire Wire Line
	2250 6850 2150 6850
Connection ~ 2250 6750
Wire Wire Line
	1150 6750 1350 6750
Wire Wire Line
	1350 6850 1250 6850
Wire Wire Line
	1250 6750 1250 6950
Connection ~ 1250 6750
Text Label 1150 6250 0    60   ~ 0
SDA
Text Label 1150 6350 0    60   ~ 0
3V3
Text Label 2350 6250 2    60   ~ 0
SCL
Text Label 2350 6350 2    60   ~ 0
INT
Wire Wire Line
	1350 6250 1150 6250
Wire Wire Line
	900  6350 1350 6350
Wire Wire Line
	2350 6250 2150 6250
Wire Wire Line
	2350 6350 2150 6350
$Comp
L GND #PWR010
U 1 1 5478D9FE
P 2400 6550
F 0 "#PWR010" H 2400 6550 30  0001 C CNN
F 1 "GND" H 2400 6480 30  0001 C CNN
F 2 "" H 2400 6550 60  0000 C CNN
F 3 "" H 2400 6550 60  0000 C CNN
	1    2400 6550
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2250 6450 2250 6650
Wire Wire Line
	2250 6650 2150 6650
Wire Wire Line
	2150 6550 2250 6550
Connection ~ 2250 6550
Wire Wire Line
	2150 6450 2400 6450
Text Label 2350 2750 0    60   ~ 0
SCL
Text Label 2350 3050 0    60   ~ 0
INT
Wire Wire Line
	2250 2750 2550 2750
Wire Wire Line
	2350 3050 2550 3050
Text Label 2350 2850 0    60   ~ 0
SDA
Wire Wire Line
	2050 2850 2550 2850
$Comp
L +3.3V #PWR011
U 1 1 54790905
P 1050 6350
F 0 "#PWR011" H 1050 6310 30  0001 C CNN
F 1 "+3.3V" H 1050 6460 30  0000 C CNN
F 2 "" H 1050 6350 60  0000 C CNN
F 3 "" H 1050 6350 60  0000 C CNN
	1    1050 6350
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG012
U 1 1 54790BBF
P 900 6350
F 0 "#FLG012" H 900 6445 30  0001 C CNN
F 1 "PWR_FLAG" H 900 6530 30  0000 C CNN
F 2 "" H 900 6350 60  0000 C CNN
F 3 "" H 900 6350 60  0000 C CNN
	1    900  6350
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG013
U 1 1 54790C24
P 2600 5650
F 0 "#FLG013" H 2600 5745 30  0001 C CNN
F 1 "PWR_FLAG" H 2600 5830 30  0000 C CNN
F 2 "" H 2600 5650 60  0000 C CNN
F 3 "" H 2600 5650 60  0000 C CNN
	1    2600 5650
	-1   0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG014
U 1 1 54790C89
P 2600 5950
F 0 "#FLG014" H 2600 6045 30  0001 C CNN
F 1 "PWR_FLAG" H 2600 6130 30  0000 C CNN
F 2 "" H 2600 5950 60  0000 C CNN
F 3 "" H 2600 5950 60  0000 C CNN
	1    2600 5950
	-1   0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG015
U 1 1 54790CEE
P 2700 6750
F 0 "#FLG015" H 2700 6845 30  0001 C CNN
F 1 "PWR_FLAG" H 2700 6930 30  0000 C CNN
F 2 "" H 2700 6750 60  0000 C CNN
F 3 "" H 2700 6750 60  0000 C CNN
	1    2700 6750
	-1   0    0    -1  
$EndComp
Connection ~ 2350 5950
Connection ~ 1050 6350
Text Notes 3700 4800 0    60   ~ 0
Addr: 0100111
$Comp
L +3.3V #PWR016
U 1 1 54791BC1
P 2350 3350
F 0 "#PWR016" H 2350 3310 30  0001 C CNN
F 1 "+3.3V" H 2350 3460 30  0000 C CNN
F 2 "" H 2350 3350 60  0000 C CNN
F 3 "" H 2350 3350 60  0000 C CNN
	1    2350 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	2350 3350 2550 3350
NoConn ~ 2550 3150
Wire Wire Line
	2450 4350 2550 4350
Wire Wire Line
	2450 3350 2450 4350
Connection ~ 2450 3350
Wire Wire Line
	2550 4150 2450 4150
Connection ~ 2450 4150
Wire Wire Line
	2550 4250 2450 4250
Connection ~ 2450 4250
$Comp
L DS18B20U U2
U 1 1 54792F88
P 7300 4500
F 0 "U2" H 7450 4550 60  0000 C CNN
F 1 "DS18B20U" V 7400 4850 60  0000 C CNN
F 2 "" H 7300 4500 60  0000 C CNN
F 3 "" H 7300 4500 60  0000 C CNN
	1    7300 4500
	-1   0    0    -1  
$EndComp
Wire Wire Line
	7750 4150 9850 4150
Wire Wire Line
	6900 3600 8300 3600
Wire Wire Line
	6900 3600 6900 4700
Wire Wire Line
	6900 4700 7300 4700
Wire Wire Line
	9850 4250 9750 4250
Connection ~ 7300 3600
$Comp
L +3V #PWR017
U 1 1 547940BB
P 10200 5450
F 0 "#PWR017" H 10200 5590 20  0001 C CNN
F 1 "+3V" H 10200 5560 30  0000 C CNN
F 2 "" H 10200 5450 60  0000 C CNN
F 3 "" H 10200 5450 60  0000 C CNN
	1    10200 5450
	1    0    0    -1  
$EndComp
Wire Wire Line
	9750 5450 10200 5450
Text Label 8950 5050 2    60   ~ 0
3V_V0
Text Label 10100 5050 2    60   ~ 0
3V_V1
Text Label 8950 4950 2    60   ~ 0
3V_V2
Text Label 10100 4950 2    60   ~ 0
3V_V3
Text Label 8950 5250 2    60   ~ 0
1V4_V
Text Label 8950 5350 2    60   ~ 0
1V4_V
Text Label 8950 4650 2    60   ~ 0
-3V_V0
Text Label 10100 4650 2    60   ~ 0
-3V_V1
Text Label 8950 4550 2    60   ~ 0
-3V_V2
Text Label 10100 4550 2    60   ~ 0
-3V_V3
$Comp
L -3V #PWR018
U 1 1 547954B3
P 10150 4350
F 0 "#PWR018" H 10150 4490 20  0001 C CNN
F 1 "-3V" H 10150 4460 30  0000 C CNN
F 2 "" H 10150 4350 60  0000 C CNN
F 3 "" H 10150 4350 60  0000 C CNN
	1    10150 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	9750 4350 10150 4350
Wire Wire Line
	8950 5050 8600 5050
Wire Wire Line
	10100 5050 9750 5050
Wire Wire Line
	8950 4950 8600 4950
Wire Wire Line
	10100 4950 9750 4950
Wire Wire Line
	10100 4550 9750 4550
Wire Wire Line
	8950 4550 8600 4550
Wire Wire Line
	10100 4650 9750 4650
Wire Wire Line
	8950 4650 8600 4650
Wire Wire Line
	8950 5350 8600 5350
Wire Wire Line
	8950 5250 8600 5250
Text Label 10000 5550 2    60   ~ 0
TDC+
Text Label 8950 5550 2    60   ~ 0
TDC-
Wire Wire Line
	8950 5550 8700 5550
Wire Wire Line
	9750 5550 10000 5550
Wire Wire Line
	1250 5550 1250 5850
Wire Wire Line
	1350 5650 1250 5650
Connection ~ 1250 5650
Text Label 7750 850  0    60   ~ 0
TDC+
Text Label 7750 950  0    60   ~ 0
TDC-
$Comp
L PWR_FLAG #FLG019
U 1 1 547A2CB4
P 6900 3600
F 0 "#FLG019" H 6900 3695 30  0001 C CNN
F 1 "PWR_FLAG" H 6900 3780 30  0000 C CNN
F 2 "" H 6900 3600 60  0000 C CNN
F 3 "" H 6900 3600 60  0000 C CNN
	1    6900 3600
	-1   0    0    -1  
$EndComp
Text Label 8200 2250 0    60   ~ 0
SCL
Wire Wire Line
	8200 2250 8400 2250
Text Label 8200 2350 0    60   ~ 0
SDA
Wire Wire Line
	8400 2350 8200 2350
$Comp
L R R67
U 1 1 547A3B4A
P 2250 2400
F 0 "R67" V 2330 2400 50  0000 C CNN
F 1 "1k" V 2250 2400 50  0000 C CNN
F 2 "" H 2250 2400 60  0000 C CNN
F 3 "" H 2250 2400 60  0000 C CNN
	1    2250 2400
	1    0    0    -1  
$EndComp
$Comp
L R R66
U 1 1 547A3BAF
P 2050 2400
F 0 "R66" V 2130 2400 50  0000 C CNN
F 1 "1k" V 2050 2400 50  0000 C CNN
F 2 "" H 2050 2400 60  0000 C CNN
F 3 "" H 2050 2400 60  0000 C CNN
	1    2050 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	2050 2850 2050 2650
Wire Wire Line
	2250 2750 2250 2650
$Comp
L +3.3V #PWR020
U 1 1 547A3E1B
P 2050 1950
F 0 "#PWR020" H 2050 1910 30  0001 C CNN
F 1 "+3.3V" H 2050 2060 30  0000 C CNN
F 2 "" H 2050 1950 60  0000 C CNN
F 3 "" H 2050 1950 60  0000 C CNN
	1    2050 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	2050 1950 2050 2150
Wire Wire Line
	2050 2050 2250 2050
Wire Wire Line
	2250 2050 2250 2150
Connection ~ 2050 2050
Text Notes 8400 2700 0    60   ~ 0
Addr: 010 0001\nAddr: 010 0010
Text Label 4650 1100 0    60   ~ 0
_EN_-3
Wire Wire Line
	4250 2750 4650 2750
Text Label 4300 2750 0    60   ~ 0
_EN_-3
Wire Wire Line
	4600 1000 5050 1000
Text Label 4650 1000 0    60   ~ 0
_EN_1V4
Wire Wire Line
	4250 2950 4650 2950
Text Label 4300 2950 0    60   ~ 0
_EN_1V4
Wire Wire Line
	1250 6950 1350 6950
Connection ~ 1250 6850
Wire Wire Line
	2250 6950 2150 6950
Connection ~ 2250 6850
Wire Wire Line
	2500 6750 2500 6700
Connection ~ 2500 6750
Wire Wire Line
	2250 5850 2150 5850
Connection ~ 2250 5750
Wire Wire Line
	1250 5850 1350 5850
Connection ~ 1250 5750
Wire Wire Line
	1250 5950 1250 6150
Wire Wire Line
	1250 6150 1350 6150
Wire Wire Line
	2250 5950 2250 6150
Wire Wire Line
	2400 6450 2400 6550
Connection ~ 2250 6450
$Comp
L GND #PWR021
U 1 1 547FFA87
P 1100 6550
F 0 "#PWR021" H 1100 6550 30  0001 C CNN
F 1 "GND" H 1100 6480 30  0001 C CNN
F 2 "" H 1100 6550 60  0000 C CNN
F 3 "" H 1100 6550 60  0000 C CNN
	1    1100 6550
	1    0    0    -1  
$EndComp
Wire Wire Line
	1250 6450 1250 6650
Wire Wire Line
	1250 6650 1350 6650
Wire Wire Line
	1350 6550 1250 6550
Connection ~ 1250 6550
Wire Wire Line
	1100 6450 1350 6450
Wire Wire Line
	1100 6450 1100 6550
Connection ~ 1250 6450
$Comp
L GND #PWR022
U 1 1 54797EAA
P 8400 5600
F 0 "#PWR022" H 8400 5600 30  0001 C CNN
F 1 "GND" H 8400 5530 30  0001 C CNN
F 2 "" H 8400 5600 60  0000 C CNN
F 3 "" H 8400 5600 60  0000 C CNN
	1    8400 5600
	-1   0    0    -1  
$EndComp
Wire Wire Line
	8400 5450 8950 5450
Wire Wire Line
	8400 4350 8400 5600
Wire Wire Line
	8950 5150 8400 5150
Connection ~ 8400 5450
Wire Wire Line
	9850 4150 9850 4250
Wire Wire Line
	8300 3600 8300 4250
Wire Wire Line
	8300 4250 8950 4250
Connection ~ 8300 3600
Wire Wire Line
	8950 4350 8400 4350
Connection ~ 8400 5150
Wire Wire Line
	8950 4450 8400 4450
Connection ~ 8400 4450
Wire Wire Line
	8950 4750 8400 4750
Connection ~ 8400 4750
Wire Wire Line
	8950 4850 8400 4850
Connection ~ 8400 4850
$Comp
L GND #PWR023
U 1 1 5490F780
P 10350 5700
F 0 "#PWR023" H 10350 5700 30  0001 C CNN
F 1 "GND" H 10350 5630 30  0001 C CNN
F 2 "" H 10350 5700 60  0000 C CNN
F 3 "" H 10350 5700 60  0000 C CNN
	1    10350 5700
	-1   0    0    -1  
$EndComp
Wire Wire Line
	10350 4450 10350 5700
Wire Wire Line
	9750 4450 10350 4450
Wire Wire Line
	9750 4750 10350 4750
Connection ~ 10350 4750
Wire Wire Line
	9750 4850 10350 4850
Connection ~ 10350 4850
Wire Wire Line
	9750 5150 10350 5150
Connection ~ 10350 5150
Wire Wire Line
	9750 5250 10350 5250
Connection ~ 10350 5250
Wire Wire Line
	9750 5350 9950 5350
Wire Wire Line
	9950 5350 9950 5250
Connection ~ 9950 5250
Wire Wire Line
	4600 900  5050 900 
Text Label 4650 900  0    60   ~ 0
_EN_3V
Wire Wire Line
	4250 4350 4700 4350
Text Label 4300 4350 0    60   ~ 0
_EN_3V
Connection ~ 4900 1500
Connection ~ 4900 1600
Connection ~ 4900 1700
Wire Wire Line
	3900 1500 5050 1500
Wire Wire Line
	4750 1600 5050 1600
Wire Wire Line
	4050 1700 5050 1700
Text Label 4050 1700 0    60   ~ 0
-3V_OC_RESET
Text Label 3900 1500 0    60   ~ 0
3V_1.4V_OC_RESET
Wire Wire Line
	4750 1600 4750 1500
Connection ~ 4750 1500
Wire Wire Line
	7550 1150 8400 1150
Text Label 7550 1150 0    60   ~ 0
TDC_OC_RESET
Wire Wire Line
	4250 3650 5100 3650
Text Label 4250 3650 0    60   ~ 0
TDC_OC_RESET
Wire Wire Line
	4250 3350 5100 3350
Text Label 4250 3350 0    60   ~ 0
-3V_OC_RESET
Wire Wire Line
	4250 3450 5100 3450
Text Label 4250 3450 0    60   ~ 0
3V_1.4V_OC_RESET
$EndSCHEMATC
