PCBNEW-LibModule-V1  Thu 20 Nov 2014 01:23:56 PM EST
# encoding utf-8
Units mm
$INDEX
SOT-363
$EndINDEX
$MODULE SOT-363
Po 0 0 0 15 546E319C 00000000 ~~
Li SOT-363
Sc 0
AR 
Op 0 0 0
T0 0 -2.667 1 1 0 0.15 N V 21 N "SOT-363"
T1 0 2.4638 1 1 1800 0.15 N V 21 N "VAL**"
DS -1.143 0.635 -1.0668 0.635 0.15 21
DS -1.143 -0.635 -1.143 0.635 0.15 21
DS -1.143 -0.635 -1.0668 -0.635 0.15 21
DS 1.143 -0.635 1.0668 -0.635 0.15 21
DS 1.143 -0.635 1.143 0.635 0.15 21
DS 1.143 0.635 1.0668 0.635 0.15 21
$PAD
Sh "4" R 0.4 0.8 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0.65 -0.95
.LocalClearance 0.127
$EndPAD
$PAD
Sh "3" R 0.4 0.8 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0.65 0.95
.LocalClearance 0.127
$EndPAD
$PAD
Sh "2" R 0.4 0.8 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0 0.95
.LocalClearance 0.127
$EndPAD
$PAD
Sh "1" R 0.4 0.8 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -0.65 0.95
.LocalClearance 0.127
$EndPAD
$PAD
Sh "5" R 0.4 0.8 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0 -0.95
.LocalClearance 0.127
$EndPAD
$PAD
Sh "6" R 0.4 0.8 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -0.65 -0.95
.LocalClearance 0.127
$EndPAD
$EndMODULE SOT-363
$EndLIBRARY
