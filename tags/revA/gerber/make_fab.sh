#!/bin/bash
#
# make zip file and template README for this project
# generate gerbers with "use proper filename extensions" UNCHECKED
# so all the gerber file names end in ".pho"
#
base="G2-LOGIC_DB"
orig="LogicBoard_DB"
cp ${orig}-B_Cu.pho ${base}-Layer4.pho
cp ${orig}-B_Mask.pho ${base}-B_Mask.pho
cp ${orig}-B_Paste.pho ${base}-B_Paste.pho
cp ${orig}-B_SilkS.pho ${base}-B_SilkS.pho
cp ${orig}.drl ${base}.drl
cp ${orig}-drl_map.pho ${base}-drl_map.pho
cp ${orig}-Dwgs_User.pho ${base}-Fab_dwg.pho
cp ${orig}-F_Cu.pho ${base}-Layer1.pho
cp ${orig}-F_Mask.pho ${base}-F_Mask.pho
cp ${orig}-F_Paste.pho ${base}-F_Paste.pho
cp ${orig}-F_SilkS.pho ${base}-F_SilkS.pho
cp ${orig}-Inner1_Cu.pho ${base}-Layer3.pho
cp ${orig}-Inner2_Cu.pho ${base}-Layer2.pho
#cp ${orig}-NPTH.drl ${base}-NPTH.drl
#cp ${orig}-NPTH-drl_map.pho ${base}-NPTH-drl_map.pho
